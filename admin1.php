<?php
$server="localhost";
$username="root";
$password="";
$connect_mysql=mysqli_connect($server,$username,$password) or die ("Connection Failed!");
$mysql_db=mysqli_select_db($connect_mysql, "redplanet") or die ("Could not Connect to Database");

$query = "SELECT * FROM bookings";
$result=mysqli_query($connect_mysql, $query) or die("Query Failed : ".mysqli_error());


$accounts = "SELECT * FROM accounts";
$users=mysqli_query($connect_mysql, $accounts) or die("Query Failed : ".mysqli_error());
?>

<script>
    
</script> 

<html>
     <head>
     <title>Red Planet Hotel | Admin Page</title> 
	
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="icon" type="image/png" href="img/rplogo.png">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
   
    <body id="adminbg">
        <div id="MainContainer">
            <div id="Header">
                    <div id="NavBar">
                        <ul>
                        <li style="color: white;">Admin Page</li>
                        </ul> 
                    </div>    
                    <div id="logo"><a href="index.php"><img src="img/logo2.png" width="250px" height="auto"></a></div>             
            </div>
        </div> 
        
         <div id="admin-wrapper" >
            <div id="page-inner">
                
                  <div class="fluid-container" style="padding: 30px; margin-top: 100px; ">
                    <center>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="" ><h4 id="textcolor">Users</h4></a></li>
                        <li style="color: white;"><a href="admin2.php" ><h4 id="textcolor">Bookings</h4></a></li>
                        <li style="color: white;"><a href="admin3.php" ><h4 id="textcolor">Availability</h4></a></li>
                        <li style="color: white;"><a href="admin4.php" ><h4 id="textcolor">Rooms</h4></a></li>
                        <li style="color: white;"><a href="admin5.php" ><h4 id="textcolor">Mesages</h4></a></li>
                    </ul>
                    </center>
                </div>
                
                    <div class="tab-content" style="background-color: beige; color: #170a02;">
                      <div role="tabpanel" class="tab-pane fade in active" id="profile" style="overflow: auto; padding: 0 55px;">
                        <table id="table" class="table table-hover">
                            <thead>
                              <tr id="panel-heading">
                                <th>Username</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th colspan=2>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                               <?php while($rows=mysqli_fetch_array($users)) { ?>
                                    <tr> 
                                        <td><?php echo $rows['username'] ?></td>
                                        <td><?php echo $rows['email'] ?></td>
                                        <td><?php echo $rows['password'] ?></td>
                                        <td><?php echo '<a href="deleteUsers.php?id='.$rows['id'].'">Delete</a>' ?></td>
                                            <?php } ?>

                                    </tr>
                            </tbody>
                          </table> 
                      </div>
                    </div>
                </div>
            </div>
    </body>
</html>

