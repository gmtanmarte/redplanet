<?php
include('config.php');

    session_start();

    if(isset($_POST['submit'])){
    $fname=$_POST['fname'];
    $email=$_POST['email'];
    $phone=$_POST['phone'];
    $troom=$_POST['troom'];
    $nroom=$_POST['nroom'];
    $meal=$_POST['meal'];
    $cin=$_POST['cin'];
    $cout=$_POST['cout'];
    
    $_SESSION['fname'] = $fname;
    $_SESSION['email'] = $email;
    $_SESSION['phone'] = $phone;
    $_SESSION['troom'] = $troom;
    $_SESSION['nroom'] = $nroom;
    $_SESSION['meal'] = $meal;
    $_SESSION['cin'] = $cin;
    $_SESSION['cout'] = $cout;
        
    
     header("location:confirmation.php");
    }
?>

<!DOCTYPE html>
<html>
    <head>
	<title>Red Planet Hotels | Book Hotel Online for Best Price</title>
	
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="icon" type="image/png" href="img/rplogo.png">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body id="aboutbg">
        <div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h2>
                            <br><br><br>
                            Reservation Form<br>
                            <h4> 1. Select Dates</h4><br>
                        </h2>
                    </div>
                </div>               
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div id="panel-primary">
                        <div id="panel-heading">
                            Booking Information
                        </div>
                        <div class="panel-body">
						
                        <form name="form" method="POST" action="reservation.php">
							  <div class="form-group">
                                            <label>Name of Guest</label>
                                            <input name="fname" class="form-control" required>   
                               </div>
                                       <div class="form-group">
                                                    <label>Email</label>
                                                    <input name="email" type="email" class="form-control" required>

                                       </div>
                                            <div class="form-group">
                                                        <label>Phone Number</label>
                                                        <input name="phone" type ="text" class="form-control" required>

                                            </div>
                                                <div class="form-group">
                                                            <label>Type of Room *</label>
                                                            <select name="troom"  class="form-control" required>
                                                                <option value selected ></option>
                    
                                                                <option value="DELUXE">DELUXE ROOM</option>
                                                                <option value="CLUB HOUSE">CLUB ROOM</option>
                                                                <option value="PREMEIRE">PREMIER ROOM</option>
                                                            </select>
                                                  </div>
                                              <div class="form-group">
                                                            <label>No.of Rooms *</label>
                                                            <select name="nroom" class="form-control" required>
                                                                <option value selected ></option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                            </select>
                                              </div>
                                            <div class="form-group">
                                                            <label>Meal Plan</label>
                                                            <select name="meal" class="form-control"required>
                                                            <option value selected ></option>
                                                            <option value="Room only">Room only</option>
                                                            <option value="Breakfast">Breakfast</option>
                                                            <option value="Full Board">Full Board</option>
                                                        </select>
                                            </div>
                                        <div class="form-group">
                                                     <label>Check-In</label>
                                                     <input name="cin" type ="date" class="form-control">
                                        </div>
                                    <div class="form-group">
                                                <label>Check-Out</label>
                                                <input name="cout" type ="date" class="form-control">
                                    </div><hr>
                            
                                    <div class="form-group">
                                                            <label><h4><b>Payment Method</b></h4></label><br>
                                                            <label>Pay with*</label><br>
                                                            
                                                            <label style="float: left; margin-right: 20px;"><input name="payment" class="form-control" type="radio" value="Paypal" style="width: 15px;" required><img src="img/visa.jpg" style="width: 80px;"></label>
                                        
                                                            <label style="float: left; margin-left: 20px;">
                                                            <input name="payment" class="form-control" type="radio" value="Paypal" style="width: 15px;" required><img src="img/paypal.png" style="width: 150px;"></label>
                                        
                                                            <label style="float: left; margin-left: 20px;"><input name="payment" class="form-control" type="radio" value="MasterCard" style="width: 15px;" required><img src="img/mastercard.jpg" style="width: 80px;"></label>
                                        
                                        <div class="form-group">
                                                        <label><br>Cardholder's Name *</label>
                                                        <input name="cardholder" type ="text" class="form-control" required>
                                            </div>
                                        
                                        <div class="form-group">
                                                        <label>Card Number *</label>
                                                        <input name="cardnumber" type ="text" class="form-control" required>

                                            </div>
                                        
                                        <label>Card Expiration (mm/yy) *<br></label>
                                        <div class="form-group">
                                                            <select name="month" class="form-control" required style=" width:60%; float:left; margin-right:5%;">
                                                                <option value selected ></option>
                                                                <option value="jan">January</option>
                                                                <option value="feb">February</option>
                                                                <option value="mar">March</option>
                                                                <option value="apr">April</option>
                                                                <option value="may">May</option>
                                                                <option value="jun">June</option>
                                                                <option value="jul">July</option>
                                                                <option value="aug">August</option>
                                                                <option value="sep">September</option>
                                                                <option value="oct">October</option>
                                                                <option value="nov">November</option>
                                                                <option value="dec">December</option>
                                                            </select>
                                              
                                                            <select name="month" class="form-control" required style=" width:35%;">
                                                                <option value selected ></option>
                                                                <option value="jan">2017</option>
                                                                <option value="feb">2018</option>
                                                                <option value="mar">2019</option>
                                                                <option value="apr">2020</option>
                                                            </select>
                                        
                                    </div>
                                <div>
                                    <input type="submit" name="submit" id="btn-primary" value="Proceed">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id="MainContainer">
            <div id="Header">
                <div id="NavBar">
                        
                        </div>    
                        <div id="logo"><a href="index.php"><img src="img/logo2.png" width="245px" height="115px"></a></div> 
                        </div>  

        </div> 
    </body>
</html>


                
        