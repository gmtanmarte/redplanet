-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2018 at 01:22 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `redplanet`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `email`, `password`) VALUES
(15, 'gm', 'gmtanmarte@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(16, 'glevinzon', 'glevinzon@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `availability`
--

CREATE TABLE IF NOT EXISTS `availability` (
  `RoomNo` int(50) NOT NULL,
  `CheckIn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CheckOut` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `troom` varchar(100) NOT NULL,
  `nroom` int(10) NOT NULL,
  `meal` varchar(100) NOT NULL,
  `cin` date NOT NULL,
  `cout` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `fname`, `lname`, `email`, `phone`, `troom`, `nroom`, `meal`, `cin`, `cout`) VALUES
(50, 'glevinzon', '', 'glevinzon@gmail.com', '123456', 'CLUB HOUSE', 2, 'Breakfast', '2018-11-28', '2018-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `clubh`
--

CREATE TABLE IF NOT EXISTS `clubh` (
  `roomnumber` int(10) NOT NULL,
  `available` int(10) NOT NULL,
  `booked` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubh`
--

INSERT INTO `clubh` (`roomnumber`, `available`, `booked`) VALUES
(401, 5, 0),
(402, 5, 0),
(403, 5, 0),
(404, 5, 0),
(405, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `deluxe`
--

CREATE TABLE IF NOT EXISTS `deluxe` (
  `roomnumber` int(10) NOT NULL,
  `available` int(10) NOT NULL,
  `booked` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deluxe`
--

INSERT INTO `deluxe` (`roomnumber`, `available`, `booked`) VALUES
(201, 5, 0),
(202, 5, 0),
(203, 5, 0),
(204, 5, 0),
(206, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `premeire`
--

CREATE TABLE IF NOT EXISTS `premeire` (
  `roomnumber` int(10) NOT NULL,
  `available` int(10) NOT NULL,
  `booked` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `premeire`
--

INSERT INTO `premeire` (`roomnumber`, `available`, `booked`) VALUES
(301, 5, 0),
(302, 5, 0),
(303, 5, 0),
(304, 5, 0),
(305, 5, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
