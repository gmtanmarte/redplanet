<!DOCTYPE html>

<?php
include('config.php');
?>

<html>
    <head>
	<title>Red Planet Hotels | Book Hotel Online for Best Price</title>
	
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="icon" type="image/png" href="img/rplogo.png">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body id="aboutbg">
        <div id="page-wrapper" >
            <div id="page-inner">
			 <div class="row">
                    <div class="col-md-12">
                        <h2>
                            <br><br><br>
                            Registration Form<br><br>
                        </h2>
                    </div>
                </div>               
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div id="panel-primary">
                        <div id="panel-heading">
                            Create an account
                        </div>
                        <div class="panel-body">
						
                        <form name="form" method="post" action="register.php">
							  
                            
                                <div class="form-group">
                                            <label>Username</label>
                                            <input name="username" type="text" class="form-control" required>   
                               </div>
                                   <div class="form-group">
                                                <label>Email</label>
                                                <input name="email" type="email" class="form-control" required>
                                   </div>
                                       <div class="form-group">
                                                    <label>Password</label>
                                                    <input name="password_1" type="password" class="form-control" required>
                                       </div>
                                            <div class="form-group">
                                                        <label>Confirm Password</label>
                                                        <input name="password_2" type ="password" class="form-control" required>
                                            </div>
                                        <div>
                                        
                                    <input type="submit" name="register" id="btn-primary">
                                                
                                        <div>
                                            <p> Already a member? <a href="index.php">Sign in</a></p>
                                        </div>  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id="MainContainer">
            <div id="Header">
                <div id="NavBar">
                  
                        <ul>
                        <li><a id="nav" href="index.php">BACK TO HOME</a></li>
                        
                       
                        </ul> 
                        
                        </div>    
                         <div id="logo"><a href="index.php"><img src="img/logo2.png" width="245px" height="115px"></a></div> 
                        </div>  

   
            </div>
            <div id="contact" class="container col-lg-12">
			<div id = "contactparagraph">
				<h3 id="textcolor" ><span><b>Contact Red Planet Hotel </b></span></h3>
				<p class="con">The best of business and pleasure. <br>Experience it all and stay at this elegant and trendy hotel in Davao.</p><br>
               
			
			<div>
			    <div class="row">
			        <div class="col-md-8">
			            <div id="panel-heading">
			                <form>
			                <div class="row">
			                    <div class="col-md-6 ">
			                        <div class="form-group">
			                            <label for="name">
			                                Name</label>
			                            <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
			                        </div>
			                         <div class="form-group">
			                            <label for="name">
			                                Contact</label>
			                            <input type="text" class="form-control" id="name" placeholder="Enter number" required="required" />
			                        </div>
			                        <div class="form-group">
			                            <label for="email">
			                                Email Address</label>
			                            <div class="input-group">
			                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
			                                </span>
			                                <input type="text" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
			                        </div>
			                    </div>
			                    <div class="col-md-6">
			                        <div class="form-group">
			                            <label for="name">
			                                Message</label>
			                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
			                                placeholder="Message"></textarea>
			                        </div>
			                    </div>
			                    <div class="col-md-12">
			                        <button type="submit" class="btn btn-primary pull-right" id="btn-send">
			                            Send Message</button>
			                    </div>
			                </div>
			                </form>
			            </div>
			        </div>
                    
			        <div class="col-md-4 offset col-md-4">
			            <form><center>
                            <p class="con">J.P. Laurel Ave,<br>
                             Buhangin, Davao City, 8000 Davao del Sur<br>
                    Phone Number: (082) 224 0577    <br>
                    Email Address: frontdesk.davao@redplanethotels.com</p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12299.162662344781!2d-77.847684!3d39.586868!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32f96c4e7c11d065%3A0xad928143b06891a6!2sRed+Planet+Hotel+Davao!5e0!3m2!1sen!2sin!4v1542220261550" width="400" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			     			

			            </center></form>
			        </div>
                    
                    </div>
                </div>	
            </div>
        </div> 
    
    </body>

</html>


                
        