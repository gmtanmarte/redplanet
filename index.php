<!DOCTYPE html>

<?php
session_start();
include('config.php');
$errors = array();

    if (isset($_POST['login'])) {
        
        $username = trim($_POST['username']);
        $password = md5($_POST['password']);
        
        $_SESSION['username'] = $username;
        
        $sql = "SELECT * FROM accounts WHERE username = '$username' AND password = '$password'";
        $result = mysqli_query($link, $sql);
        $data = mysqli_fetch_assoc($result);
    
        if(empty($errors)) {
            
            if($username == $data['username'] && $password == $data['password']) {
                
                $_SESSION["id"] = $data['id'];
                header("location: guest.php");
            }else {
            
                $errors["match_form"] = "Username/Password do not match";
            }
        }
        mysqli_close($link);
    }

?>

<html>
    <head>
	<title>Red Planet Hotels | Book Hotel Online for Best Price</title> 
	
        <link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="icon" type="image/png" href="img/rplogo.png">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,600" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!-- UIkit CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.34/css/uikit.min.css" />

        <!-- UIkit JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.34/js/uikit.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.34/js/uikit-icons.min.js"></script>


        <script>
            $(document).ready(function(){
                // Add smooth scrolling to all links
                $("a").on('click', function(event) {

                    // Make sure this.hash has a value before overriding default behavior
                    if (this.hash !== "") {

                        // Prevent default anchor click behavior
                            event.preventDefault();

                            // Store hash
                            var hash     = this.hash;

                            // Using jQuery's animate() method to add smooth page scroll
                            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                            $('html, body').animate({
                            scrollTop: $(hash).offset().top
                            }, 1500, function(){

                            // Add hash (#) to URL when done scrolling (default click behavior)
                             window.location.hash = hash;
                            });
                        } // End if
                });
            });

        </script>
	
    </head>
    
    <body>
            

        <a id="home"></a>
        
        <!-- -----------------Carousel------------------- -->
        
        <div class="uk-grid">
        
              <div id="myCarousel" class="carousel slide .uk-width-*" data-ride="carousel">
                <!--Indicators -->
                
                <ol class="carousel-indicators">
		        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#myCarousel" data-slide-to="1"></li>
		        <li data-target="#myCarousel" data-slide-to="2"></li>
                
                </ol>   

                <!-- Wrapper for slides -->
                <div class="carousel-inner">

                  <div class="item active">
                    <img src="img/cover.jpg" alt="Los Angeles" style="width:100%;">
                  </div>

                  <div class="item">
                    <img src="img/cover1.jpg" alt="Chicago" style="width:100%;">
                  </div>

                  <div class="item">
                    <img src="img/cover2.jpg" alt="New York" style="width:100%;">
                  </div>

                   
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <!-- -----------------End of Carousel------------------- -->
        
            <div id="MainContainer" >
        
            <div id="Header">
            
                    <div id="NavBar">
                   
                        <ul>
                        <li><a id="nav" href="register.php">REGISTER</a></li>
                        <li><a id="nav" href="#login">LOG IN</a></li>
                        <li><a id="nav" href="#contact">CONTACT</a></li>
                        <li><a id="nav" href="#about">ABOUT</a></li>
                        <li><a id="nav" href="#home">HOME</a></li>
                        </ul> 
                        
                        </div>    
                        <div id="logo"><a href="index.php"><img src="img/logo2.png" width="250px" height="20px"></a></div> 
                        </div>
                        <a id="login"></a>
                        </div>
                    </div>   
                    
                    <div id="aboutbg">
                    <div style="width:270px; margin:0 auto; margin-top: 100px; margin-bottom: 100px;">   
                        
                        <form action="index.php" method="post">
                            <div id="#aboutparagraph">
                            
                            <h3 id="textcolor" style="margin-top: 100px;" ><span><b>Sign in here </b></span></h3>
                            Username:<br><input type="text" name="username" id = "uname" class="form-control" required/><br  />
                            Password:<br><input type="password" name="password" id="password" class="form-control" required/><br  />
                            <input type="submit" name="login" id ="btn-login" value="Sign-in" class="form-control" required  />
                            <div><center>
                                <p style="margin-top: 10px;"> Not a member? <a href="register.php">Register here</a></p>
                                </center>
                            </div>  
                            
                            </div>
                        </form> 
                    </div>
                    </div>
        
                    <div id="reservebg">
                        
                        <center>
                        <h3 id="textcolor" style="margin-top: 100px;" ><span><b>A warm smile from a familiar face. </b></span></h3>
                            
                        <h4 id="textcolor" style="margin-top: 2px;"><span>Discover the best feeling in the world and experience genuine hospitality.</span></h4><br><br><br><br>
                        </center>
                        
                            <div style="margin-right: 200px; text-align: justify; ">
                                
                                <img src="img/deluxe.jpg" width="700px" style="float: left; margin-left: 50px; margin-right: 20px; margin-bottom: 50px;">
                                
                                <p> <h4><b><br>Deluxe Rooms</b></h4>Deluxe Rooms are executed in soothing earth tones that evoke a warm and restful ambiance. At 28sqm, each is equipped with a king or twin beds, a 40-inch LED HDTV with cable channels, media panel with HDMI, USB and audio-visual connectivity, complimentary Wi-Fi and broadband internet access, IDD phone with iPod dock, alarm clock, voice mail and radio,coffee and tea making facilities, mini-bar and in-room safe.<br><br><br><br><br><br><br><br>
                            </div>
                        
                              <div style="margin-left: 200px; text-align:justify; ">
                                
                                <img src="img/clubroom.jpg" width="700px" style="float: right; margin-right: 50px; margin-left: 20px; margin-bottom: 50px;">
                                
                                <p> <h4 style="text-align:right;"><b><br><br>Club Rooms</b></h4>Club Rooms offer a range of privileges for discerning travelers. Located on the 10th and 11th floors, they provide access to the Club Lounge where guests enjoy exclusivity and premium service; daily Continental breakfast; all-day refreshments and cocktails; iMac stations; work tables; reading materials; a wide-screen LED HDTV with cable channels; and secretarial assistance.<br><br><br><br><br><br><br><br><br><br><br><br>
                            </div>
                        
                            <div style="margin-right: 200px; text-align: justify; ">
                                
                                <img src="img/premiere.jpg" width="700px" style="float: left; margin-left: 50px; margin-right: 20px; margin-bottom: 50px;">
                                
                                <p> <h4><b><br><br>Premier Rooms</b></h4>Premier Rooms offer bigger accommodations and supplementing facilities. With a floor area of 35 sqm, additional features include a bath tub and kitchenette, as well as Club Lounge access.safe.<br><br><br><br><br><br><br><br>
                            </div>
                
                        <br><br><br>

                    </div>
        
        
            <a id="about"></a>
                    <div id="aboutbg">
                        
                        <div id="aboutparagraph">
                            
                            <h3 id="textcolor" ><span><b>About Red Planet Hotel</b></span></h3>
                            <p> Red Planet Hotels Limited (RPHL) was founded in 2010 and has quickly established itself as Asia’s largest and fastest-growing hotel company focused on the budget hotel sector. The company owns and operates hotels in the Philippines, Indonesia, Thailand, and Japan and plans to continue its expansion across Asia.
                              At Red Planet, we focus on what is important to our customers: location, value, and cleanliness. All Red Planet hotels are:

                             Located in the city centre, close to public transport
                              Priced at competitive rates with free high-speed Wi-Fi
                             Well maintained and spotlessly clean

                              Half of our brand experience is designed around our leading-edge and customer-focused technology platform. Our mobile app and In-Stay Mode make booking rooms and communicating with hotel staff easy.

                             When our guests leave our hotels, our aim is to have made them feel as if they had made a smart decision by staying with us. We are all about ensuring value for our customers. 
                            </p>
                            
                        </div>
                        
                        
                        <br><br><br><br><br><br><br><br><br>

                        </div>
        
                        
        	<div id="contact" class="container col-lg-12">
			<div id = "contactparagraph">
				<h3 id="textcolor" ><span><b>Contact Red Planet Hotel </b></span></h3>
				<p class="con">The best of business and pleasure. <br>Experience it all and stay at this elegant and trendy hotel in Davao.</p><br>
               
			
			<div>
			    <div class="row">
			        <div class="col-md-8">
			            <div id="panel-heading">
			                <form>
			                <div class="row">
			                    <div class="col-md-6 ">
			                        <div class="form-group">
			                            <label for="name">
			                                Name</label>
			                            <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
			                        </div>
			                         <div class="form-group">
			                            <label for="name">
			                                Contact</label>
			                            <input type="text" class="form-control" id="name" placeholder="Enter number" required="required" />
			                        </div>
			                        <div class="form-group">
			                            <label for="email">
			                                Email Address</label>
			                            <div class="input-group">
			                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
			                                </span>
			                                <input type="text" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
			                        </div>
			                    </div>
			                    <div class="col-md-6">
			                        <div class="form-group">
			                            <label for="name">
			                                Message</label>
			                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
			                                placeholder="Message"></textarea>
			                        </div>
			                    </div>
			                    <div class="col-md-12">
			                        <button type="submit" class="btn btn-primary pull-right" id="btn-send">
			                            Send Message</button>
			                    </div>
			                </div>
			                </form>
			            </div>
			        </div>
                    
			        <div class="col-md-4 offset col-md-4">
			            <form><center>
                            <p class="con">J.P. Laurel Ave,<br>
                             Buhangin, Davao City, 8000 Davao del Sur<br>
                    Phone Number: (082) 224 0577    <br>
                    Email Address: frontdesk.davao@redplanethotels.com</p>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12299.162662344781!2d-77.847684!3d39.586868!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32f96c4e7c11d065%3A0xad928143b06891a6!2sRed+Planet+Hotel+Davao!5e0!3m2!1sen!2sin!4v1542220261550" width="400" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
			     			

			            </center></form>
			        </div>
                    
                    </div>
                </div>	
            </div>
        </div>     
	</body>


</html>
